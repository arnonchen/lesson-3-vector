#include "Vector.h"
#include <array>
#include <iostream>
Vector::Vector(int n)
{
	if (n < 2)
		n = 2;
	if (!Vector::empty())
	{
		_capacity = n;
		_size = 0;
		_resizeFactor = n;
		_elements = new int[n];
	}
	else
	{
		int* newVector = new int[n];
		for (int i = 0; i < _capacity; i++)
			newVector[i] = _elements[i];
		_capacity += n;
		_elements = newVector;
	}
}
Vector::~Vector()
{
	_elements = nullptr;
}

int Vector::size() const
{
	return _size;
}

int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{

	return _resizeFactor;
}

bool Vector::empty() const
{
	if (_elements)
		return true;
	return false;
}

void Vector::push_back(const int& val)
{
	int* elements = 0;

	if (_capacity == _size)
	{
		elements = new int[_capacity + _resizeFactor];
		for (int i = 0; i < _capacity; i++)
			elements[i] = _elements[i];
		_capacity += _resizeFactor;
		_elements = elements;
	}
	_elements[_size + 1] = val;
	_size++;
}

int Vector::pop_back()
{
	int returned_value = 0;
	if (_size == 0)
	{
		std::cout << "error: pop from empty vector";
		return -9999;
	}
	returned_value = _elements[_size];
	_size--;
	return returned_value;
}

void Vector::reserve(int n)
{
	int i = 0;
	int* elements = 0;
	if (_capacity >= n)
		return;
	while (_capacity < n)
		_capacity += _resizeFactor;
	elements = new int[_capacity];
	for (int i = 0; i < _capacity; i++)
		elements[i] = _elements[i];
	_elements = elements;
}

void Vector::resize(int n)
{
	if (n <= _capacity)
		_size = n;
	else
	{
		Vector::reserve(n);
		_size = n;
	}
}

void Vector::assign(int val)
{
	for (int i = 0; i <= _size; i++)
	{
		_elements[i] = val;
	}
}
void Vector::resize(int n, const int& val)
{
	Vector::resize(n);
	Vector::assign(val);
}

Vector::Vector(const Vector& other)
{
	this->_capacity = other._capacity;
	this->_elements = new int[other._capacity];
	for (int i = 0; i < other._size; i++)
		this->_elements[i] = other._elements[i];
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
}

Vector& Vector::operator=(const Vector& other)
{
	this->_capacity = other._capacity;
	this->_elements = new int[other._capacity];
	for (int i = 0; i < other._size; i++)
		this->_elements[i] = other._elements[i];
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	return *this;
}

int& Vector::operator[](int n) const
{
	int returned_value;
	if (n > _size)
	{
		std::cout << "error: n bigger than the size, returning first index";
		return *_elements;
	}
	return *(_elements + n);
}